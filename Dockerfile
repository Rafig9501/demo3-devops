# the first stage of our build will use a maven 3.6.1 parent image
FROM maven:3.6.3-jdk-11 AS MAVEN_BUILD

# ARG JAR_FILE=target/*.jar
WORKDIR /app

# copy the pom and src code to the container
COPY src /app/src

# copy pom.xml to the container's WORKDIR
COPY pom.xml /app

# package our application code
RUN mvn clean package

# the second stage of our build will use open jdk 8 on alpine 3.9
FROM openjdk:11

WORKDIR /app

# copy only the artifacts we need from the first stage and discard the rest
COPY --from=MAVEN_BUILD /app/target/spring-petclinic-2.3.1.BUILD-SNAPSHOT.jar ./demo.jar

# set the startup command to execute the jar
CMD ["java", "-jar", "-Dspring.profiles.active=mysql", "demo.jar"]

#