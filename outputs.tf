output "rds_address" {
  value = aws_db_instance.petclinicrdsdemo3.address
}

output "ec2_address" {
  value = aws_instance.pet_clinic_java_ec2.public_ip
}