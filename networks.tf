resource "aws_vpc" "pet_clinic_vpc" {
  cidr_block = "33.3.0.0/16"

  tags = {
    Name = "pet-clinic-vpc-demo3"
  }
}

resource "aws_internet_gateway" "pet_clinic_ig" {
  vpc_id = aws_vpc.pet_clinic_vpc.id

  tags = {
    Name = "pet-clinic-ig-demo3"
  }
}

resource "aws_route_table" "pet_clinic_rt" {
  vpc_id = aws_vpc.pet_clinic_vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.pet_clinic_ig.id
  }

  tags = {
    Name = "pet-clinic-rt-demo3"
  }
}

resource "aws_subnet" "pet_clinic_public_subnet_demo3" {
  vpc_id = aws_vpc.pet_clinic_vpc.id
  cidr_block = "33.3.1.0/24"
  availability_zone = "us-east-1a"
  map_public_ip_on_launch = true
  tags = {
    "Name" = "pet-clinic-public-subnet-demo3"
  }
}

resource "aws_subnet" "pet_clinic_private_subnet1_demo3" {
  vpc_id = aws_vpc.pet_clinic_vpc.id
  cidr_block = "33.3.2.0/24"
  availability_zone = "us-east-1a"

  tags = {
    "Name" = "pet-clinic-private-subnet-demo3"
  }
}

resource "aws_subnet" "pet_clinic_private_subnet2_demo3" {
  vpc_id = aws_vpc.pet_clinic_vpc.id
  cidr_block = "33.3.3.0/24"
  availability_zone = "us-east-1b"

  tags = {
    "Name" = "pet-clinic-private-subnet-demo3"
  }
}

resource "aws_route_table_association" "pet_clinic_rt_subnet_association_demo3" {
  subnet_id = aws_subnet.pet_clinic_public_subnet_demo3.id
  route_table_id = aws_route_table.pet_clinic_rt.id
}

resource "aws_db_subnet_group" "pet_clinic_rds_sg_demo3" {
  name = "pet_clinic_rds_sg_demo3"
  subnet_ids = [
    aws_subnet.pet_clinic_private_subnet1_demo3.id,
    aws_subnet.pet_clinic_private_subnet2_demo3.id]

  tags = {
    Name = "pet_clinic_db_subnet_group_demo3"
  }
}


resource "aws_security_group" "pet_clinic_sg_demo3" {
  vpc_id = aws_vpc.pet_clinic_vpc.id

  ingress {
    description = "HTTP from VPC"
    from_port = 80
    to_port = 80
    protocol = "tcp"
    cidr_blocks = [
      "0.0.0.0/0"]
  }

  ingress {
    description = "HTTP from VPC"
    from_port = 3306
    to_port = 3306
    protocol = "tcp"
    cidr_blocks = [
      "0.0.0.0/0"]
  }

  ingress {
    description = "SSH for VPC"
    from_port = 22
    protocol = "tcp"
    to_port = 22
    cidr_blocks = [
      "0.0.0.0/0"]
  }

  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = [
      "0.0.0.0/0"]
    ipv6_cidr_blocks = [
      "::/0"]
  }

  tags = {
    Name = "pet_clinic_java_sg_demo3"
  }
}