import os
import urllib.request
import time

url = f'http://{os.environ["PUBLIC_IP"]}:80//actuator/health'

for i in range(10):
    status_code = urllib.request.urlopen(url).getcode()
    if status_code == 200:
        print("Application is up")
        break
    else:
        print(f"URL is unavailable: {url}")
        time.sleep(10)
else:
    print("Application is down")
    exit(1)
