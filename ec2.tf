resource "aws_instance" "pet_clinic_java_ec2" {
  ami = "ami-0e902fc6a00c4d5ba"
  instance_type = "t2.medium"
  availability_zone = "us-east-1a"

  subnet_id = aws_subnet.pet_clinic_public_subnet_demo3.id
  security_groups = [
    aws_security_group.pet_clinic_sg_demo3.id]

  tags = {
    Name = "WEB-SERVER"
  }

  user_data = <<EOF
                  #!/bin/sh
                  docker pull registry.gitlab.com/rafig9501/demo3-devops:latest; echo SPRING_DATASOURCE_USERNAME=${aws_db_instance.petclinicrdsdemo3.username} > /root/env-file; echo SPRING_DATASOURCE_PASSWORD=${aws_db_instance.petclinicrdsdemo3.password} >> /root/env-file; echo SPRING_DATASOURCE_URL=jdbc:mysql://${aws_db_instance.petclinicrdsdemo3.address}/${aws_db_instance.petclinicrdsdemo3.name} >> /root/env-file; echo SPRING_PROFILES_ACTIVE=mysql >> /root/env-file; docker run --env-file=/root/env-file -d -p 80:8080 --name petclinic registry.gitlab.com/rafig9501/demo3-devops
                  EOF
}