resource "aws_db_instance" "petclinicrdsdemo3" {
  allocated_storage = 20
  engine = "mysql"
  engine_version = "8.0"
  instance_class = "db.t2.medium"
  name = "petclinicrdsdemo3"
  username = var.MYSQL_USERNAME
  password = var.MYSQL_PASSWORD
  storage_type = "gp2"
  publicly_accessible = false
  multi_az = false
  max_allocated_storage = 21
  skip_final_snapshot = true
  identifier = "petclinicrdsdemo3"
  db_subnet_group_name = aws_db_subnet_group.pet_clinic_rds_sg_demo3.name
  vpc_security_group_ids = [
    aws_security_group.pet_clinic_sg_demo3.id]
}
