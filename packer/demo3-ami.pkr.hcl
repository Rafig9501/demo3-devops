variable "aws_access_key" {
  type = string
  description = "The username for authenticating to vCenter."
  default = ""
  sensitive = true
}
variable "aws_secret_key" {
  type = string
  description = "The username for authenticating to vCenter."
  default = ""
  sensitive = true
}
variable "ssh_username" {
  type = string
  description = "The username to use to authenticate over SSH."
  default = ""
  sensitive = true
}

variable "region" {
  type = string
  description = "The plaintext password to use to authenticate over SSH."
  default = ""
}
variable "ami_name" {
  type = string
  description = "The plaintext password to use to authenticate over SSH."
  default = ""
}
variable "instance_type" {
  type = string
  description = "The plaintext password to use to authenticate over SSH."
  default = ""
}

source "amazon-ebs" "ec2-java-ejdaha" {
  access_key = var.aws_access_key
  secret_key = var.aws_secret_key
  ssh_username = var.ssh_username
  ami_name = var.ami_name
  instance_type = var.instance_type
  region = var.region
  communicator = "ssh"
  ssh_timeout = "5m"
  force_deregister = true
  force_delete_snapshot = true

  source_ami_filter {
    filters = {
      virtualization-type = "hvm"
      name = "amzn2-ami-hvm-2.0.*-ebs"
      root-device-type = "ebs"
    }

    owners = [
      "137112412989"]
    most_recent = true
  }

  launch_block_device_mappings {
    device_name = "/dev/xvda"
    volume_size = 8
    volume_type = "gp2"
    delete_on_termination = true
  }
}

build {
  sources = [
    "source.amazon-ebs.ec2-java-ejdaha"
  ]

  provisioner "shell" {
    inline = [
      "sudo yum update -y",
      "sudo yum install -y java",
      "sudo yum install -y docker",
      "sudo systemctl enable docker"
    ]
  }
}
